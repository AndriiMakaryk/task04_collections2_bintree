package task04_Collections2_BinTree;

public class TesterBinaryTree {
    public static void main(String[] args) {
        BinaryTree clinic = new BinaryTree();
        clinic.addNode(50,"Head doctor");
        clinic.addNode(15,"HR manager");
        clinic.addNode(25,"Vicehead doctor");
        clinic.addNode(30,"Secretary");
        clinic.addNode(75,"Lower");
        clinic.addNode(85,"Privisor");
        clinic.addNode(80,"Cheef of Department1");
        clinic.addNode(40,"Cheef of Department2");
        clinic.addNode(27,"Cheef of Department3");
        clinic.addNode(85,"Cheef of Department4");
        System.out.println();
        clinic.inOrderTraverseTree(clinic.root);
        System.out.println("Find position by key 80.");
        System.out.println(clinic.findNodeByKey(80));
        System.out.println("Find position by key 30.");
        System.out.println(clinic.findNodeByKey(30));
        System.out.println();
        System.out.println("Delete Node by key 50.");
        System.out.println(clinic.removeNodeByKey(50));

    }
}
