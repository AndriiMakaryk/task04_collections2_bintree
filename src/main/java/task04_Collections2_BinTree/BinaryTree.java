package task04_Collections2_BinTree;

public class BinaryTree {
    Node root;

    class Node {
        int key;
        String name;
        Node leftChild;
        Node rightChild;

        Node(int key, String name) {
            this.key = key;
            this.name = name;
        }

        @Override
        public String toString() {
            return "Node [key=" + key + ", name=" + name + ", leftChild=" + leftChild + ", rightChild=" + rightChild
                    + "]";
        }
    }

    public void addNode(int key, String name) {
        Node newNode = new Node(key, name);
        if (root == null) {
            root = newNode;
        } else {
            Node focusNode = root;
            Node parent;
            while (true) {
                parent = focusNode;
                if (key < focusNode.key) {
                    focusNode = focusNode.leftChild;
                    if (focusNode == null) {
                        parent.leftChild = newNode;
                        return;
                    }
                } else {
                    focusNode = focusNode.rightChild;
                    if (focusNode == null) {
                        parent.rightChild = newNode;
                        return;
                    }
                }
            }
        }
    }

    public Node findNodeByKey(int key) {
        Node focusNode = root;
        while (focusNode.key != key) {
            if (key < focusNode.key) {
                focusNode = focusNode.leftChild;
            } else {
                focusNode = focusNode.rightChild;
            }
            if (focusNode == null) return null;
        }
        return focusNode;
    }

    public boolean removeNodeByKey(int key) {
        Node focusNode = root;
        Node parent = root;
        boolean isInAChild = true;
        while (focusNode.key != key) {
            parent = focusNode;
            if (key < focusNode.key) {
                isInAChild = true;
                focusNode = focusNode.leftChild;
            } else {
                isInAChild = false;
                focusNode = focusNode.rightChild;
            }
            if (focusNode.leftChild == null && focusNode.rightChild == null) {
                if (focusNode == root) {
                    root = null;
                } else if (isInAChild) {
                    parent.leftChild = null;
                } else {
                    parent.rightChild = null;
                }
            } else if (focusNode.rightChild == null) {
                if (focusNode == root) {
                    root = focusNode.leftChild;
                } else if (isInAChild) {
                    parent.leftChild = focusNode.leftChild;
                } else {
                    parent.rightChild = focusNode.leftChild;
                }
            } else if (focusNode.leftChild == null) {
                if (focusNode == root)
                    root = focusNode.rightChild;
                else if (isInAChild)
                    parent.leftChild = focusNode.rightChild;
                else
                    parent.rightChild = focusNode.leftChild;
            } else {
                Node replacement = getReplacementNode(focusNode);
                if (focusNode == root) {
                    root = replacement;
                } else if (isInAChild) {
                    parent.leftChild = replacement;
                } else {
                    parent.rightChild = replacement;
                    replacement.leftChild = focusNode.leftChild;
                }
            }
        }
        return true;
    }

    public Node getReplacementNode(Node replacedNode) {
        Node replacementParent = replacedNode;
        Node replacement = replacedNode;
        Node focusNode = replacedNode.rightChild;
        while (focusNode != null) {
            replacementParent = replacement;
            replacement = focusNode;
            focusNode = focusNode.leftChild;
        }
        if (replacement != replacedNode.rightChild) {
            replacementParent.leftChild = replacement.rightChild;
            replacement.rightChild = replacedNode.rightChild;
        }
        return replacement;
    }

    public void inOrderTraverseTree(Node focusNode) {
        if (focusNode != null) {
            System.out.println(focusNode);
            inOrderTraverseTree(focusNode.leftChild);
            inOrderTraverseTree(focusNode.rightChild);
        }
    }
}
